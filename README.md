# Ejemplo de implementación del patrón MVC 

Este repositorio contiene una implementación de MVC en PHP

## Cómo ejecutar

1. Copiar el archivo `config/app.example.php` en `config/app.php`. Este será el archivo utilizado por la aplicación para la configuración.
2. Ejecutar el servidor que viene con la interfáz de línea de comandos de PHP:

    ```bash
    php -S 127.0.0.1:8080
    ```

3. En el navegador dirigirse a [http://127.0.0.1:8080](http://127.0.0.1:8080).

## Pendientes

* Capa de persistencia (Clase de persistencia)
* Manejo de sesiones y cookies (clase de sesiones y cookies)
* Incorporación de templates (Clase de templates)
