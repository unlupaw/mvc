<?php
namespace UNLu\PAW\Libs;
use UNLu\PAW\Libs\Router;
use UNLu\PAW\Libs\PeticionHTTP;
/**
 * Atiende una petición
 *
 * @author Santiago Ricci <sricci.soft at gmail.com>
 */
class Despachador {
    
    private $router;
    
    public function __construct(Router $router) {
        $this->router = $router;
    }

    public function desapchar(PeticionHTTP $request){                
        $ruta = $request->getUri();
        $peticion = $this->router->route($ruta);        
        /* @var $controlador Controlador */
        $controlador = $this->getInstanciaControlador($peticion['controlador']);        
        if(key_exists('accion',$peticion) && method_exists($controlador, $peticion['accion'])){
            if(!key_exists('parametros', $peticion)){
                $peticion['parametros'] = [];
            }            
            $controlador->ejecutarAccion($peticion['accion'], $peticion['parametros']);
        }else{
            die('Error 404');
        }
    }        
    
    private function getInstanciaControlador($controlador){
        $className = "UNLu\\PAW\\Controladores\\" . ucfirst($controlador);
        if(class_exists($className)){
            return new $className();
        }else{
            die("Controlador no existente: $className");
        }
    }
}
