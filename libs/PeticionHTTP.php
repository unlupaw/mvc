<?php
namespace UNLu\PAW\Libs;

/**
 * Petición HTTP
 *
 * @author sriccisoft
 */
class PeticionHTTP {
    private $uri;
    
    private $method;
    
    
    public function __construct($serverArr) {        
        $this->uri = isset($serverArr['REQUEST_URI']) ? $serverArr['REQUEST_URI'] : '';
        $this->method = $serverArr['REQUEST_METHOD'];
    }
    
    public function getUri() {
        return $this->uri;
    }

    public function getMethod() {
        return $this->method;
    }

}
